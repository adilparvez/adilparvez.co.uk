var nodemailer = require('nodemailer');
var directTransport = require('nodemailer-direct-transport');

var transporter = nodemailer.createTransport(directTransport({
  name: 'adilparvez.co.uk'
}));

var mailOptions = {
  from: 'donotreply@adilparvez.co.uk',
  subject: 'Eid-e-claus'
};

function send(to, text, callback) {
  mailOptions.to = to;
  mailOptions.text = text;
  transporter.sendMail(mailOptions, callback);
}

exports.send = send;