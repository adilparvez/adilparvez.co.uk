function buildGroups(body) {
  var groups = {};

  Object.keys(body).forEach(function(tag) {
    var groupId = tag.split('-')[0];
    if (!groups[groupId]) {
      groups[groupId] = [];
    }
    groups[groupId].push(body[tag]);
  });
  return groups;
}

function isPossible(groups) {
  var possible = true;
    Object.keys(groups).forEach(function(group) {
      if (groups[group].length > collapse(groups, group).length) {
        possible = false;
      }
    });
    return possible;
}

function collapse(groups, excludedGroup) {
  var people = [];
  Object.keys(groups).forEach(function(group) {
    if (group != excludedGroup) {
      groups[group].forEach(function (person) {
        people.push(person);
      });
    }
  });
  return people;
}

function getRandomElementWithoutReplacement(array) {
  return array.splice(Math.floor(Math.random() * array.length), 1)[0];
}

function contains(array, element) {
  var i = array.length;
  while (i--) {
     if (array[i] == element) {
         return true;
     }
  }
  return false;
}

function getPairings(groups) {
  var pairs = {};
  var taken = [];
  var start = new Date().getTime();
  Object.keys(groups).forEach(function(group) {
    groups[group].forEach(function(person) {
      var candidates = collapse(groups, group);
      var chosen;
      do {
        chosen = getRandomElementWithoutReplacement(candidates);
        if (new Date().getTime() - start >= 500) {
          break;
        }
      } while (contains(taken, chosen));
      taken.push(chosen);
      pairs[person] = chosen;
    });
  });
  return pairs;
}

function areAllDefined(pairs) {
  var allDefined = true;
  Object.keys(pairs).forEach(function(key){
    if (pairs[key] == undefined) {
      allDefined = false;
    }
  });
  return allDefined;
}

function isIrreflexive(pairs) {
  var isIrreflexive = true;
  Object.keys(pairs).forEach(function(key) {
    if (pairs[key] == key) {
      isIrreflexive = false;
    }
  });
  return isIrreflexive;
}

function draw(groups) {
  var pairs;
  while (true) {
    pairs = getPairings(groups);
    if (isIrreflexive(pairs) && areAllDefined(pairs)) {
      return pairs;
    }
  }
}

exports.isPossible = isPossible;
exports.buildGroups = buildGroups;
exports.draw = draw;