var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var schema = mongoose.Schema({
  username: String,
  name: String,
  password: String,
  email: String,
  wishlist: String,
  hasChangedInitialPassword: Boolean,
  hasPermission: Boolean
});

schema.statics.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
}

schema.methods.isValidPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
}

module.exports = mongoose.model('User', schema);