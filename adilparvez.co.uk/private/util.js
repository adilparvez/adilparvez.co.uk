var fs = require('fs');

function ls(directory) {
  var files = [];
  fs.readdirSync(directory).forEach(function (file) {
    files.push(file);
  });
  return files;
}

exports.ls = ls;