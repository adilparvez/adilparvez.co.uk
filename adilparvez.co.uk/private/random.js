var crypto = require('crypto');

function getHexToken(length) {
  if (length % 2 != 0) {
    ++length;
  }
  return crypto.randomBytes(length/2).toString('hex');
}

exports.getHexToken = getHexToken;