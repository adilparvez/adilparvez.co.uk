var LocalStrategy = require('passport-local').Strategy;
var User = require('../db/models/user');

module.exports = function(passport) {
  passport.use('eid-e-claus-login', new LocalStrategy({
    passReqToCallback: true
  },
  function(req, username, password, done) {
    process.nextTick(function() {
      User.findOne({
        username: username
      },
      function(err, user) {
        if (err) {
          return done(err);
        }
        if (!user) {
          return done(null, false, req.flash('error', 'User not found'));
        }
        if (!user.isValidPassword(password)) {
          return done(null, false, req.flash('error', 'Wrong password'));
        }
        return done(null, user);
      });
    });
  }));
};