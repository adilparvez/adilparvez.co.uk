var User = require('../db/models/user');
var login = require('./eid-e-claus-login');
var signup = require('./eid-e-claus-signup');

module.exports = function(passport) {
  passport.serializeUser(function(user, done) {
    done(null, user._id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  login(passport);
  signup(passport);
};