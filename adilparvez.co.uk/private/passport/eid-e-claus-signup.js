var LocalStrategy = require('passport-local').Strategy;
var User = require('../db/models/user');
var mailer = require('../mailer');
var random = require('../random');

module.exports = function(passport) {
  passport.use('eid-e-claus-signup', new LocalStrategy({
    passReqToCallback: true,
    passwordField: 'email'
  },
  function(req, username, email, done) {
    process.nextTick(function() {
      User.findOne({
        username: username
      },
      function(err, user) {
        if (err) {
          console.log(err);
          return done(err);
        }
        if (user) {
          return done(null, false, req.flash('error', 'User already exists'));
        }
        var newUser = new User();
        newUser.username = username;
        newUser.name = req.body.name;
        var password = random.getHexToken(16);
        newUser.password = User.generateHash(password);
        newUser.email = email;
        newUser.wishlist = '';
        newUser.hasChangedInitialPassword = false;
        newUser.hasPermission = false;
        mailer.send(email, 'Your password is: ' + password, function(err, info) {
          if (!err) {
            newUser.save(function(err) {
              if (err) {
                console.log(err);
                return done(null, false, req.flash('error', 'Error creating account'));
              } else {
                return done(null, newUser, req.flash('success', 'An email has been sent with further instructions'));
              }
            });
          } else {
            return done(null, false, req.flash('error', 'Error sending email'));
          }
        });
      });
    });
  }));
};