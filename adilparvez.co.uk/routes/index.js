var express = require('express');
var router = express.Router();
var User = require('../private/db/models/user');
var draw = require('../private/draw');
var mailer = require('../private/mailer');
var util = require('../private/util');

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    res.status(401);
    res.render('401');
  }
}

function ensureHasPermission(req, res, next) {
  if (req.user.hasPermission) {
    return next();
  } else {
    res.status(401);
    res.render('401');
  }
}

module.exports = function(passport) {
  router.get('/', function(req, res, next) {
    res.render('index');
  });

  router.get('/visualiser', function(req, res) {
    res.render('visualiser', {
      files: util.ls(__dirname + '/../public/download/music')
    });
  });

  router.get('/eid-e-claus-login', function(req, res) {
    if (req.isAuthenticated()) {
      res.render('eid-e-claus-wishlist');
    } else {
      res.render('eid-e-claus-login', {
        error: req.flash('error'),
        success: req.flash('success')
      });
    }
  });

  router.post('/eid-e-claus-login', function(req, res, next) {
    passport.authenticate('eid-e-claus-login', function(err, user, info) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return res.redirect('/eid-e-claus-login');
      }
      req.logIn(user, function(err) {
        if (err) {
          return next(err)
        }
        if (user.hasChangedInitialPassword) {
          return res.redirect('/eid-e-claus-wishlist');
        } else {
          return res.redirect('/eid-e-claus-change-password');
        }
      });
    })(req, res, next);
  });

  router.get('/eid-e-claus-signup', function(req, res) {
    res.render('eid-e-claus-signup', {
      error: req.flash('error')
    });
  });

  router.post('/eid-e-claus-signup', function(req, res, next) {
    passport.authenticate('eid-e-claus-signup', function(err, user, info) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return res.redirect('/eid-e-claus-signup');
      }
      return res.redirect('/eid-e-claus-login');
    })(req, res, next);
  });

  router.get('/eid-e-claus-wishlist', ensureAuthenticated, function(req, res) {
    res.render('eid-e-claus-wishlist', {
      hasPermission: req.user.hasPermission,
      wishlist: req.user.wishlist,
      error: req.flash('error'),
      success: req.flash('success')
    });
  });

  router.get('/eid-e-claus-account', ensureAuthenticated, function(req, res) {
    res.render('eid-e-claus-account', {
      hasPermission: req.user.hasPermission,
      name: req.user.name,
      error: req.flash('error'),
      success: req.flash('success')
    });
  });

  router.post('/eid-e-claus-logout', ensureAuthenticated, function(req, res) {
    req.logout();
    req.session.destroy();
    res.render('eid-e-claus-login', {
      error: '',
      success: 'Successfully logged out'
    });
  });

  router.post('/eid-e-claus-wishlist', ensureAuthenticated, function(req, res) {
    req.user.wishlist = req.body.wishlist;
    req.user.save(function(err) {
      if (err) {
        req.flash('error', 'Error saving wishlist');
      } else {
        req.flash('success', 'Successfully saved wishlist');
      }
      res.redirect('/eid-e-claus-wishlist');
    });
  });

  router.get('/eid-e-claus-change-password', ensureAuthenticated, function(req, res) {
    res.render('eid-e-claus-change-password', {
      error: req.flash('error')
    });
  });

  router.post('/eid-e-claus-change-password', ensureAuthenticated, function(req, res) {
    if (req.body.password === req.body.confirmPassword) {
      req.user.password = User.generateHash(req.body.password);
      req.user.hasChangedInitialPassword = true;
      req.user.save(function(err) {
        if (err) {
          req.flash('error', 'Error changing password');
          res.redirect('/eid-e-claus-change-password');
        } else {
          req.flash('success', 'Successfully changed password');
          res.redirect('/eid-e-claus-wishlist');
        }
      });
    } else {
      req.flash('error', 'Passwords don\'t match');
      res.redirect('/eid-e-claus-change-password');
    }
  });

  router.post('/eid-e-claus-change-name', ensureAuthenticated, function(req, res) {
    req.user.name = req.body.name;
    req.user.save(function(err) {
        if (err) {
          req.flash('error', 'Error changing name');
          res.redirect('/eid-e-claus-account');
        } else {
          req.flash('success', 'Successfully changed name');
          res.redirect('/eid-e-claus-account');
        }
      });
  });

  router.post('/eid-e-claus-delete-account', ensureAuthenticated, function(req, res) {
    req.user.remove(function(err) {
      if (err) {
          req.flash('error', 'Error deleting account');
          res.redirect('/eid-e-claus-account');
        } else {
          req.flash('success', 'Successfully deleted account');
          res.redirect('/eid-e-claus-login');
        }
    });
  });

  router.get('/eid-e-claus-draw', ensureAuthenticated, ensureHasPermission, function(req, res) {
    User.find({}, '_id name username', function(err, users) {
      res.render('eid-e-claus-draw', {
        people: users,
        hasPermission: req.user.hasPermission,
        error: req.flash('error'),
        success: req.flash('success')
      });
    });
  });

  router.post('/eid-e-claus-draw', ensureAuthenticated, ensureHasPermission, function(req, res) {
    var groups = draw.buildGroups(req.body);
    if (draw.isPossible(groups)) {
      var pairs = draw.draw(groups);
      Object.keys(pairs).forEach(function(person) {
        User.findById({_id: person}, function(err, me) {
          if (err) {
            console.log(err);
          } else {
            User.findById({_id: pairs[person]}, function(err, you) {
              if (err) {
                console.log(err);
              } else {
                var emailText = me.name + ' gets ' + you.name + '.\n' +
                  you.name + '\'s wishlist:\n' +
                  you.wishlist;
                mailer.send(you.email, emailText, function(err, info) {
                  if (err) {
                    console.log(err);
                  }
                });
              }
            });
          }
        });
      });
      req.flash('success', 'Emails sent');
      res.redirect('/eid-e-claus-draw');
    } else {
      req.flash('error', 'No pairing exists');
      res.redirect('/eid-e-claus-draw');
    }
  });

  return router;
};