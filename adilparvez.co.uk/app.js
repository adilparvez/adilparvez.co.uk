var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var dbConfig = require('./private/db/config');
var mongoose = require('mongoose');
mongoose.connect(dbConfig.url);

var app = express();

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(favicon(__dirname + '/public/images/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(__dirname + '/public'));

var passport = require('passport');
var session = require('express-session');

app.use(session({
  resave: false,
  saveUninitialized: false,
  secret: process.env.SECRET
}));
app.use(passport.initialize());
app.use(passport.session());

var flash = require('connect-flash');
app.use(flash());

var initializePassport = require('./private/passport/initialize-passport');
initializePassport(passport);

var routes = require('./routes/index')(passport);
app.use('/', routes);

app.use(function(req, res, next) {
  res.status(404);
  res.render('404');
});

module.exports = app;