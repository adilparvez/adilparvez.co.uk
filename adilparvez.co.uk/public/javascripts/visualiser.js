function Animation(canvasId, render, extra) {
  var self = this;
  
  var canvas = document.getElementById(canvasId);
  var context = canvas.getContext('2d');
  var isRunning = false;

  self.extra = extra;

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = (function (callback) {
    return window.webkitRequestAnimationFrame ||
           window.mozRequestAnimationFrame ||
           window.oRequestAnimationFrame ||
           window.msRequestAnimationFrame ||
           function (callback) {
             window.setTimeout(callback, 1000 / 60);
           };
    })();
  }

  self.clear = function() {
    context.clearRect(0, 0, canvas.width, canvas.height);
  };
  
  self.start = function() {
    if (!isRunning) {
      isRunning = true;
      draw();
    }
  };

  self.stop = function() {
    isRunning = false;
  };

  function draw() {
    if (isRunning) {
      self.clear();
      render(context, self.extra);
      requestAnimationFrame(draw);
    }
  }

}

function Audio(audioContext, url) {
  var self = this;

  var isPaused = true;
  var startedAt;
  var pausedAt;
  var source;

  self.analyser = audioContext.createAnalyser();
  self.analyser.fftSize = 4096;
  self.analyser.minDecibels = -140;
  self.analyser.maxDecibels = 0;
  self.analyser.connect(audioContext.destination);

  var bufferLength = self.analyser.frequencyBinCount;
  var timeData = new Uint8Array(bufferLength);
  var frequencyData = new Uint8Array(bufferLength);

  self.decode = function (onDecoded) {
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'arraybuffer';

    request.onload = function() {
      audioContext.decodeAudioData(request.response, function (buffer) {
        self.buffer = buffer;
        onDecoded();
      });
    }

    request.send();
  };

  self.startPlayback = function() {
    isPaused = false;
    source = audioContext.createBufferSource();
    source.buffer = self.buffer;
    source.connect(self.analyser);
    source.loop = true;
    if (pausedAt) {
      startedAt = Date.now() - pausedAt;
      source.start(0, pausedAt / 1000);
    } else {
      startedAt = Date.now();
      source.start(0);
    }
  };

  self.isPaused = function() {
    return isPaused;
  }

  self.stopPlayback = function() {
    if (source) {
      isPaused = true;
      source.stop(0);
      pausedAt = Date.now() - startedAt;
    }
  };

  self.togglePlayback = function() {
    if (isPaused) {
      self.startPlayback();
    } else {
      self.stopPlayback();
    }
  };

  self.restartPlayback = function() {
    self.stopPlayback();
    pausedAt = 0;
    self.startPlayback();
  };

  self.getTimeData = function() {
    self.analyser.getByteTimeDomainData(timeData);
    return timeData;
  };

  self.getFrequencyData = function() {
    self.analyser.getByteFrequencyData(frequencyData);
    return frequencyData;
  };

}

function render(context, extra) {
  var frequencyData = extra.audio.getFrequencyData();
  for (var i = 0; i < frequencyData.length; ++i) {
    var factor = frequencyData[i] / 256;
    var height = extra.canvasHeight * factor;
    var offset = extra.canvasHeight - height;
    var width = extra.canvasWidth / frequencyData.length;
    var hue = i / frequencyData.length * 360;
    context.fillStyle = 'hsl(' + hue + ', 100%, 50%)';
    context.fillRect(i * width, offset, width, height);
  }

  var timeData = extra.audio.getTimeData();
  context.beginPath();
  context.moveTo(0, extra.canvasHeight - extra.canvasHeight * timeData[0] / 256 - 1);
  for (var i = 1; i < timeData.length; ++i) {
    var factor = timeData[i] / 256;
    var height = extra.canvasHeight * factor;
    var offset = extra.canvasHeight - height;
    var width = extra.canvasWidth / timeData.length;
    context.lineWidth = 1;
    context.strokeStyle = '#ffffff';
    context.lineTo(i * width, offset);
  }
  context.stroke();
};


function initialise(audioContext, file) {
  var audio = new Audio(audioContext, '//adilparvez.co.uk/download/music/' + file);
  audio.decode(function () {
    var extra = {
      audio: audio,
      canvasWidth: 1280,
      canvasHeight: 720
    };
    var animation = new Animation('myCanvas', render, extra);
    animation.start();
    $('#toggle').prop('disabled', false);
    $('#restart').prop('disabled', false);
  });
  return audio;
}

$(document).ready(function() {
  var audio;
  var audioContext = new (window.AudioContext || window.webkitAudioContext)();

  var toggle = $('#toggle');
  var restart = $('#restart');
  var fileSelect = $('#fileSelect');

  toggle.click(function() {
    if (audio) {
      audio.togglePlayback();
      audio.isPaused() ? toggle.html('play') : toggle.html('pause');
    }
  });

  restart.click(function() {
    if (audio) {
      audio.restartPlayback();
      toggle.html('pause');
    }
  });

  fileSelect.change(function() {
    toggle.prop('disabled', true);
    restart.prop('disabled', true);
    if (audio) {
      audio.stopPlayback();
    }
    toggle.html('play');
    audio = initialise(audioContext, fileSelect.val());
  });

});