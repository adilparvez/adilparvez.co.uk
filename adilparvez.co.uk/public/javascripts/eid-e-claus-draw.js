(function($) {

  var groupId = 0;
  var personId = 0;

  var addGroup = function(groupId) {
    var group = '<div id="group-' + groupId + '">' +
                  '<div class="form-group">' +
                    '<div class="col-md-10 col-md-offset-1">' +
                      '<button type="button" class="btn btn-primary" id="addPerson-' + groupId + '"">Add person</button>' +
                    '</div>' +
                  '</div>' +
                '</div>' +
                '<hr>'
    $('#names').append(group);
    $('#addPerson-' + groupId).click(function() {
      var select = '<div class="form-group">' +
                     '<div class="col-md-10 col-md-offset-1">' +
                       '<select class="form-control" name="' + groupId + '-' + personId + '">';
      select += '<option disabled selected>choose person</option>';
      for (var i = 0; i < people.length; ++i) {
        var person = people[i];
        select += '<option value="' + person._id + '">' + person.name + ' (' + person.username + ')</option>';
      }
      select += '</select>' +
              '</div>' +
            '</div>';
      $('#group-' + groupId).append(select);
      personId++;
    });
  };

  var bindListeners = function() {
    $('#addGroup').click(function() {
      addGroup(groupId);
      groupId++;
    });
  };

  $(function() {
    bindListeners();
  });
    
})(jQuery);